from django.db import models
from datetime import (
    datetime,
    timedelta,
    )

FORM_SUBMISSION_TOLERANCE = 120

class FormSubmissionManager(models.Manager):
    def check_hash(self, hash):
        submission_tolerance = datetime.now() - timedelta(seconds=FORM_SUBMISSION_TOLERANCE)
        qs = super(FormSubmissionManager, self).filter(registered__lt=submission_tolerance).delete()
        obj, created = super(FormSubmissionManager, self).get_or_create(hash=hash)
        return created

class FormSubmission(models.Model):
    hash = models.CharField(max_length=32)
    registered = models.DateTimeField(auto_now_add=True)

    objects = FormSubmissionManager()

    def __str__(self):
        return '%s. %s' % (self.id, self.registered)
