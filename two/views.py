from django.shortcuts import render

# submit page
def submit(request):
    return render(request, 'two/submit.html')
