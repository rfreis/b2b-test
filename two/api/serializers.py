from django.utils.crypto import get_random_string
from rest_framework.serializers import (
    ModelSerializer,
    ValidationError,
    )
from one.models import Profile
from ..models import FormSubmission

class ProfileCreateSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'id',
            'name',
            'email',
        ]

    def create(self, validated_data):
        name = validated_data['name']
        email = validated_data['email']
        submit_hash = name + email
        first_submission = FormSubmission.objects.check_hash(submit_hash)
        if first_submission == False:
            raise ValidationError("This data has been already submitted")
        obj = Profile.objects.create(name=name, email=email)
        return obj
