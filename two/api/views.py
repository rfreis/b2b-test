from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from one.models import Profile
from .serializers import ProfileCreateSerializer

class ProfileCreateAPIView(CreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileCreateSerializer
    permission_classes = [AllowAny]
