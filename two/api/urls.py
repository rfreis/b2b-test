from django.conf.urls import url

from .views import (
    ProfileCreateAPIView,
    )

urlpatterns = [
    url(r'^create/$', ProfileCreateAPIView.as_view(), name='profile-create'),
]
