from django.conf.urls import url
from . import views

app_name = 'two'

urlpatterns = [
    url(r'^submit/$', views.submit, name='submit'),
]
