#!/bin/bash

# Absolute path to this script, e.g. /home/user/Documents/test/install.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/Documents/test
SCRIPTPATH=$(dirname "$SCRIPT")
echo "- Abrindo pasta local: $SCRIPTPATH"
cd $SCRIPTPATH

echo "- Criando virtualenv"
virtualenv testeenv

echo "- Abrindo virtualenv"
source testeenv/bin/activate

echo "- Instalando requerimentos"
pip install -r requirements.txt

echo "- Criando migrações"
python manage.py makemigrations one two

echo "- Aplicando migrações no banco de dados"
python manage.py migrate

echo "- Rotina de instalação completa!"
