$.i18n().load( {
  'en' : {
    "test-title": "Local vs Internationalization",
    "test-currency": "$",
    "test-back": "Back",
  },
  'pt-br' : {
    "test-title": "Local vs Internacionalização",
    "test-currency": "R$",
    "test-back": "Voltar",
  },
} );
