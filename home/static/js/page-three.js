// switch language
function switch_locale(language) {
  $.i18n({
    locale: language
  });
  $('body').i18n()
}
