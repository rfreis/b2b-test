// search profile by name and email
function get_list_profile(page=1) {
  // get search keywords
  var search = $("#id_search").val();
  // turn spinner visible
  loader("on");
  // send get request
  $.get(list_profile_api(search, page), function( data ) {
    // save data returned
    var qs_data = data.results;
    // iterate data array
    for (i in qs_data) {
      // save item into variable p
      var p = qs_data[i];
      // save data into variable insert_html: id, name and email
      var insert_html = '<tr>';
      insert_html += add_td(p.id);
      insert_html += add_td(p.name);
      insert_html += add_td(p.email);
      insert_html += '</tr>';
      // display insert_html data
      $('#profile_list > tbody').append(insert_html);
    }
    // pagination
    // if there is not other pages
    if (data.next == null) { var pages = page; }
    // elseif, there are multiple pages
    else { var pages = data.count / qs_data.length; }
    // if there is not data returned
    if (qs_data.length == 0) {
      var insert_html = '<tr>';
      insert_html += add_td('This search has returned 0 results.', null, 3, 'center');
      insert_html += '</tr>';
      $('#profile_list > tbody').append(insert_html);
    }
    // display pagination
    var call_function = 'get_list_profile_page';
    var insert_pagination = get_pagination(pages, page, call_function)
    $('div.pagination').append(insert_pagination);
    // hide spinner
    loader("off");
  }).fail(function(xhr, status, error) {
    // hide spinner
    loader("off");
    // show error messages
    if (error == 'Forbidden') {
      alert("Forbidden access.");
    }
    else {
      alert("Something has gone wrong. Please contact support.");
      console.log(xhr);
    }
  });
}
// reload pagination details for list_profile function
function get_list_profile_page(page){
  $('#profile_list > tbody:last-child').empty();
  $('div.pagination').empty();
  get_list_profile(page);
}

// submit search
$("#search_profile").submit(function(event){
  event.preventDefault();
  get_list_profile_page(1)
});
