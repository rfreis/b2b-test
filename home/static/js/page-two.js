// submit search
$("#submit_form").submit(function(event){
  // cancel default submit
  event.preventDefault();
  // check is data has been already submitted
  if (typeof(form_submitted) == "undefined") {
    // blocks new submission for 120s
    form_submitted = true;
    delay(function(){
      delete form_submitted;
    }, 120 );
    // get form into variable
    var form = $(this);
    // get create profile url
    var url = create_profile_api();
    // set request method
    var method = 'POST';
    // create request
    var request = send_request(method, url, form.serialize());
    // if request has been successful
    request.done(function( data ) {
      console.log(data);
      alert("Profile has been successfully created")
    });
    // elseif, has failed
    request.fail(function( jqXHR, textStatus, error ) {
      console.log(jqXHR);
      console.log("Request failed: " + textStatus);
      console.log("Request failed: " + error);
      if (error == "Bad Request") {
        alert(jqXHR.responseJSON[0]);
      }
      else {
        alert("Something has gone wrong. Please contact support.");
      }
    });
  }
  else {
    alert("This form has already been submitted.")
  }
});
