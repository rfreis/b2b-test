// return api root
function api_url(url) { return '/api' + url }

// return api's url for list profile
function list_profile_api(qs=null, page=1) {
  var link = '/one/?page=' + page;
  if (qs != null) { link += '&search=' + qs }
  return api_url(link);
}
// return api's url for create profile
function create_profile_api() {
  var link = '/two/create/';
  return api_url(link);
}

// load pagination
function get_pagination(pages, active, call_function) {
  pages = Math.ceil(pages);
  if (active == 1) { var dis = 'class="disabled"' }
  var prev = active - 1;
  var next = active + 1;
  var html = '<nav><ul class="pagination"><li ' + dis + '><a href="javascript:;" onclick="' + call_function + '(' + prev + ')">Anterior</a></li>'
  for (i = 1; i <= pages; i++) {
    if (i == active) { var act = 'class="active"'}
    else { var act = '' }
    html += '<li ' + act + '><a href="javascript:;" class="' + i + '" onclick="' + call_function + '(' + i + ')">' + i + '</a></li>'
  }
  if (active == pages) { var dis2 = 'class="disabled"' }
  html += '<li ' + dis2 + '><a href="javascript:;" onclick="' + call_function + '(' + next + ')">Próximo</a></li></ul></nav>'
  return html
}

// add td
function add_td(inner, td_class=null, colspan=null, textalign=null, tags=null){
  if (inner == null) { inner = '-'; }
  var insert_html = '';
  if (td_class != null){
    insert_html += ' class="' + td_class + '"';
  }
  if (colspan != null){
    insert_html += ' colspan="' + colspan + '"';
  }
  if (textalign != null){
    insert_html += ' style="text-align:' + textalign + '"';
  }
  if (tags != null) {
    insert_html += ' ' + tags;
  }
  return '<td' + insert_html + '>' + inner + '</td>';
}

// show/hide loader
function loader(action="on") {
  if (action == "on") { $("#loading").show(); }
  else { $("#loading").hide(); }
}

// delay function
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

// send ajax request (put or post)
function send_request(method, url, data){
  return $.ajax({
    method: method,
    url: url,
    data: data,
    headers:
    {
        'X-CSRFToken': $('input[name=csrfmiddlewaretoken]').val()
    }
  });
}
