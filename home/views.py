from django.shortcuts import render

# Welcome page
def welcome(request):
    return render(request, 'home/welcome.html')
