from rest_framework.filters import SearchFilter
from rest_framework.generics import (
    ListAPIView,
    )
from rest_framework.permissions import AllowAny
from ..models import Profile
from .pagination import DefaultNumberPagination
from .serializers import ProfileSerializer

class ProfileListAPIView(ListAPIView):
    serializer_class = ProfileSerializer
    filter_backends= [SearchFilter]
    search_fields = ['name', 'email']
    pagination_class = DefaultNumberPagination
    permission_classes = [AllowAny]

    def get_queryset(self, *args, **kwargs):
        queryset_list = Profile.objects.all().order_by('-id')
        return queryset_list
