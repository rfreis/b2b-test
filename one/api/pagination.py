from rest_framework.pagination import (
    PageNumberPagination,
    )

class DefaultNumberPagination(PageNumberPagination):
    page_size = 2
