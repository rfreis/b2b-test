from django.shortcuts import render

# Search page
def search(request):
    return render(request, 'one/search.html')
