from django.db import models

class Profile(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, null=True, blank=True)

    def __str__(self):
        return '%s. %s' % (self.id, self.name)
