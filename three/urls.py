from django.conf.urls import url
from . import views

app_name = 'three'

urlpatterns = [
    url(r'^translate/$', views.translate, name='translate'),
]
