from django.shortcuts import render

# locale vs Internationalization page
def translate(request):
    return render(request, 'three/translate.html')
