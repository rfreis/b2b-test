from django.contrib import admin
from django.urls import path
from django.conf.urls import (
    include,
    url
    )
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    # home urls
    url(r'^', include('home.urls')),
    # one urls
    url(r'^one/', include('one.urls')),
    url(r'^api/one/', include('one.api.urls')),
    # two urls
    url(r'^two/', include('two.urls')),
    url(r'^api/two/', include('two.api.urls')),
    # three urls
    url(r'^three/', include('three.urls')),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
